package yum_session

import (
	"sync"
)

type Session interface {
	// Set session value
	Set(key string, value interface{})
	// Get session value
	Get(key string) (interface{}, bool)
	// Delete session value, call save function to take effect
	Delete(key string) interface{}
	// Get the current session id
	SessionID() string
}

func newStore(sid string, expired int64, values map[string]interface{}) *yumSession {
	if values == nil {
		values = make(map[string]interface{})
	}

	return &yumSession{
		sid:     sid,
		expired: expired,
		values:  values,
	}
}

type yumSession struct {
	sync.RWMutex
	sid     string
	expired int64
	values  map[string]interface{}
}

func (s *yumSession) Set(key string, value interface{}) {
	s.Lock()
	s.values[key] = value
	s.Unlock()
}

func (s *yumSession) Get(key string) (interface{}, bool) {
	s.RLock()
	val, ok := s.values[key]
	s.RUnlock()
	return val, ok
}

func (s *yumSession) SessionID() string {
	return s.sid
}

func (s *yumSession) Delete(key string) interface{} {
	s.RLock()
	v, ok := s.values[key]
	s.RUnlock()

	if ok {
		s.Lock()
		delete(s.values, key)
		s.Unlock()
	}
	return v
}
