package vector

import "errors"

// any type
type T struct{}

type vector []T

func (v vector) last() (T, error) {
	var zero T
	if len(v) == 0 {
		return zero, errors.New("empty")
	}
	return v[len(v)-1], nil
}
