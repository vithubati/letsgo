package vector

import (
	"fmt"
	"testing"
	"unicode/utf8"
)

func Test_vector(t *testing.T) {
	fmt.Print("vectorInterface : ")
	vItf := vectorInterface{10, "A", 20, "B", 3.14, "B"}
	itf, err := vItf.last()
	switch v := itf.(type) {
	case int:
		fmt.Println("integer: ")
	case string:
		fmt.Println("string: ")
	default:
		fmt.Printf("unknown type %T: ", v)
	}
	fmt.Printf("value: %v error: %v\n", itf, err)
}

func Test_vector2(t *testing.T) {
	fmt.Print("vectorInterface : ")
	vItf := vectorInterface{10, "A", 20, "B", 3.14}
	itf, err := vItf.last()
	switch v := itf.(type) {
	case int:
		if v < 0 {
			fmt.Println("negative integer: ")
		}
	case string:
		if !utf8.ValidString(v) {
			fmt.Println("non-valid string: ")
		}
	default:
		fmt.Printf("unknown type %T: ", v)
	}
	fmt.Printf("value: %v error: %v\n", itf, err)
}