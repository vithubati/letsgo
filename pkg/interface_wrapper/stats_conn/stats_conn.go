package stats_conn

import (
	"fmt"
	"net"
)

//ref - https://eli.thegreenplace.net/2020/embedding-in-go-part-3-interfaces-in-structs/
//thanks to method forwarding.
// StatsConn is a `net.Conn` that counts the number of bytes read
// from the underlying connection.
type StatsConn struct {
	net.Conn

	BytesRead uint64
}

func (sc *StatsConn) Read(p []byte) (int, error) {
	n, err := sc.Conn.Read(p)
	sc.BytesRead += uint64(n)
	return n, err
}


func (sc *StatsConn) Close() error {
	fmt.Println("StatsConn close called")
	return sc.Conn.Close()
}

