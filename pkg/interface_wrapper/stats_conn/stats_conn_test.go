package stats_conn

import (
	"fmt"
	"sort"
	"testing"
)

func TestStatsConn_Read(t *testing.T) {
	lst := []int{4, 5, 2, 8, 1, 9, 3}
	sort.Sort(sort.IntSlice(lst))
	fmt.Println(lst)

	sort.Sort(sort.Reverse(sort.IntSlice(lst)))
	fmt.Println(lst)
}
