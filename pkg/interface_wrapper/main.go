package main

import (
	"bitbucket.org/vithubati/letsgo/pkg/interface_wrapper/stats_conn"
	"fmt"
	"io/ioutil"
	"log"
	"net"
)

func main() {
	//readByBytes()
	readAll()
}

func readByBytes()  {
	conn, err := net.Dial("tcp", "localhost:3306")
	if err != nil {
		log.Fatal(err)
	}
	sconn := &stats_conn.StatsConn{
		Conn:      conn,
		BytesRead: 0,
	}
	defer sconn.Close()
	p := make([]byte, 20)
	for {
		_, err := sconn.Read(p)
		if err != nil {
			log.Println(err)
			return
		}
		fmt.Println(string(p))
		fmt.Println("Bytes read: ", sconn.BytesRead)
	}
}

func readAll() {
	conn, err := net.Dial("tcp", "localhost:3306")
	if err != nil {
		log.Fatal(err)
	}
	sconn := &stats_conn.StatsConn{
		Conn:      conn,
		BytesRead: 0,
	}

	defer sconn.Close()
	//p := make([]byte, 4)
	resp, err := ioutil.ReadAll(sconn)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println(string(resp))
	fmt.Println("Bytes read: ", sconn.BytesRead)
	return
}