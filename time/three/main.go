package main

import (
	"fmt"
	"time"
)

func main() {
	ticker := time.NewTicker(time.Second)
	go func() {
		for {
			select {
			case ticker, ok := <-ticker.C:
				if ok {
					fmt.Println(ticker.String())
					doSomething()
				}
			}
		}
	}()

	time.Sleep(time.Second * 4)
	ticker.Stop()
	fmt.Println("Ticker stopped")
}

func doSomething() {
	fmt.Println("Hi There...")
}
