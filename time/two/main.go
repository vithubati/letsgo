package main

import (
	"fmt"
	"time"
)

func main() {
	// Intervals, sleeping, and tickers
	doSomethingWithRateLimit()
}

func doSomethingWithRateLimit() {
	ticker := time.NewTicker(2 * time.Second)
	for range ticker.C {
		// doSomething() is executed every 2 second forever
		doSomething()
	}
}

func doSomething()  {
	fmt.Println("Hi There...")
}