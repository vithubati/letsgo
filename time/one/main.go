package main

import (
	"fmt"
	"log"
	"time"
)

func main() {
	myTime := time.Now()
	myTimezone := myTime.Location()
	fmt.Println(fmt.Sprintf("myTimezone %+v", myTimezone))
	fmt.Println(myTime)

	// Convert a time from one location to another
	lkLocation, err := time.LoadLocation("Asia/Colombo")
	if err != nil {
		log.Fatal(err)
	}
	lkTime := myTime.In(lkLocation)
	fmt.Println(lkTime)
	fmt.Println(lkLocation)

	// Add, subtract and compare times
	// inTenMinutes is 10 minutes in the future
	inTenMinutes := myTime.Add(time.Minute * 10)
	fmt.Println(inTenMinutes)

	// tenMinutesAgo is 10 minutes in the past
	tenMinutesAgo := myTime.Add(-time.Minute * 10)
	fmt.Println(tenMinutesAgo)

	// adds years, months, and days
	// inOneMonth is 1 month in the future
	inOneMonth := myTime.AddDate(0, 1, 0)
	fmt.Println(inOneMonth)

	// twoDaysAgo is 2 days in the past
	twoDaysAgo := myTime.AddDate(0, 0, -2)
	fmt.Println(twoDaysAgo)

	// Get difference between two times
	start := time.Date(2020, 2, 1, 3, 0, 0, 0, time.UTC)
	end := time.Date(2021, 2, 1, 12, 0, 0, 0, time.UTC)

	difference := end.Sub(start)
	fmt.Printf("difference = %v\n", difference)

	//	Compare two times to see which comes after the other
	first := time.Date(2020, 2, 1, 3, 0, 0, 0, time.UTC)
	second := time.Date(2021, 2, 1, 12, 0, 0, 0, time.UTC)

	isFirstAfter := first.After(second)
	fmt.Println(isFirstAfter)

	first = time.Date(2020, 2, 1, 3, 0, 0, 0, time.UTC)
	second = time.Date(2021, 2, 1, 12, 0, 0, 0, time.UTC)

	// equal is true if the both times refer to the same instant
	// two times are equal even if they are in different locations
	equal := first.Equal(second)
	fmt.Println(equal)

}
