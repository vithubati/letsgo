package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan int)

	go func() {
		for i := 0; i < 5; i++ {
			time.Sleep(2 * time.Second)
			fmt.Println("assigning Next value")
			//fmt.Println("address of ch is : ", &ch)
			ch <- i
		}
		close(ch)
	}()

	for v := range ch {
		//fmt.Println("address of v is : ", &ch)
		fmt.Printf("Next value is: %T --- %d", v, v)
		fmt.Println("\nwaiting for the Next value")
	}
}
