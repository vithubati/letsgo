package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan int)
	go func() {
		time.Sleep(5 * time.Second)
		ch <- 55
	}()

	fmt.Println("=====gonna  wait until valus being assigned to channel ")
	fmt.Println(<-ch)
	fmt.Println("=====done----")
}
