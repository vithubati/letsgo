package main

import "fmt"

func main() {
	od := make(chan int)
	ev := make(chan int)
	qt := make(chan int)

	// send()
	go send(od, ev, qt)
	// receive()
	receive(od, ev, qt)

	fmt.Println("about to quit")

}

func send(od, ev, qt chan<- int) {
	for i := 0; i < 5; i++ {
		if i%2 == 0 {
			ev <- i
		} else {
			od <- i
		}
	}
	qt <- -1
}

func receive(od, ev, qt <-chan int) {
	for {
		select {
		case v:= <-od:
			fmt.Println("From the ODD Channel: ", v)
		case v:= <-ev:
			fmt.Println("From the Even Channel: ", v)
		case v:= <-qt:
			fmt.Println("From the Quit Channel: ", v)
			return
		}
	}
}