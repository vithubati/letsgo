package main

func main() {
	concurrency := 5
	pool := make(chan struct{}, concurrency)

	select {
	case pool <- struct{}{}:
		go func() {
			defer func() { <-pool }()
			// Do concurrent thing.
		}()
	}
}
