package main

import (
	"fmt"
	"time"
)

var sendChan chan bool

func SendValue() {
	sendChan = make(chan bool, 1)
	go func() {
		sendChan <- Send()
		close(sendChan)
	}()

	select {
	case r := <-sendChan:
		fmt.Println("returned result from Send...", r)
	case <-time.After(time.Second):
		fmt.Println("timed out returning...")
		return
	}
	//continue logic in case send didn't timeout
}
func Send() bool {
	time.Sleep(2 * time.Second)
	fmt.Println("returning from Send...")
	return true
}
func main() {
	SendValue()
	time.Sleep(5 * time.Second)
	_, ok := <-sendChan
	fmt.Println(fmt.Sprintf("channes is closed? %v", !ok))
	_, ok = <-sendChan
	fmt.Println(fmt.Sprintf("channes is closed? %v", !ok))
}
