package main

import (
	"fmt"
	"time"
)

func main() {
	od := make(chan int)
	ev := make(chan int)
	qt := make(chan bool)

	// send()
	go send(od, ev, qt)
	// receive()
	receive(od, ev, qt)

	fmt.Println("about to quit")

}

func send(od, ev chan<- int, qt chan<- bool) {
	for i := 0; i < 5; i++ {
		if i%2 == 0 {
			time.Sleep(2 * time.Second)
			fmt.Println("Sending to Even Channel: ", i)
			ev <- i
		} else {
			time.Sleep(2 * time.Second)
			fmt.Println("Sending to ODD Channel: ", i)
			od <- i
		}
	}
	//close(ev)
	//close(od)
	close(qt)
}

func receive(od, ev <-chan int, quit <-chan bool) {
	defer fmt.Println("receive done")
	for {
		select {
		case v := <-od:
			fmt.Println("Receiving from ODD Channel: ", v)
		case v := <-ev:
			fmt.Println("Receiving from Even Channel: ", v)
		//	comma ok idiom
		case i, ok := <-quit:
			if !ok {
				fmt.Println("from comma ok", i, ok)
				return
			} else {
				fmt.Println("from comma ok", i)
			}
		}
	}
}
