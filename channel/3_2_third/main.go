package main

import "fmt"

func main() {
	ch := make(chan int)
	go send(ch)
	receive(ch)
}

func send(ch chan<- int) {
	ch <- 45
}

func receive(ch <-chan int) {
	fmt.Println(<- ch)
}
