package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan int, 1)
	go func() {
		ch <- getNumber()
	}()

	fmt.Println("waiting for the getNumber() to return...")
	fmt.Println(<-ch)
	fmt.Println("DONE")
	//fmt.Println(<-ch)
	//fmt.Println("=====done----")
}

func getNumber() int {
	time.Sleep(5 * time.Second)
	return 55
}
