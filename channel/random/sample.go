package main

import (
	"fmt"
	"time"
)

func main() {
	var c chan string
	c = make(chan string)
	go func() {
		for {
			c <- "let's get started"
			time.Sleep(1 * time.Second)
			close(c)
			return
		}
	}()
	go func() {
			select {
			case x, ok := <-c:
				if !ok{
					fmt.Println(x, ok)
					return
				}
				fmt.Println(x, ok)
			}
	}()
	time.Sleep(2 * time.Second)
	fmt.Println("Done")
}
