package main

import (
	"fmt"
	"time"
)

func main()  {
	ch := make(chan int, 1)
	go func() {
		ch <- 55
		ch <- 45
	}()
	//time.Sleep(1 * time.Second)
	ch <- 35

	//fmt.Println("=====gonna  wait until valus being assigned to channel ")
	time.Sleep(5 * time.Second)
	fmt.Println(<-ch)
	fmt.Println(<-ch)
	fmt.Println(<-ch)
	//fmt.Println(<-ch)
	//fmt.Println("=====done----")
}
