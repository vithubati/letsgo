package main

import "fmt"

type Person struct {
	Name *Name
	Age int
}

func (p Person) GetFullName() string {
	return p.Name.Firstname
}

func (p Person) SetFullName()  {
	p.Name.Firstname = "Moorthy"
}

type Name struct {
	Firstname string
	Lastname string
}
func main() {
	p := Person{
		Name: &Name{
			Firstname: "Ratna",
			Lastname:  "",
		},
		Age:  15,
	}
	fmt.Println(fmt.Sprintf("Name is %s", p.GetFullName()))
	p.SetFullName()
	fmt.Println(fmt.Sprintf("Name is %s", p.GetFullName()))
}
