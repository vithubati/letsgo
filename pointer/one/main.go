package main

import "fmt"

func main() {

	pointer1()
}

func pointer1() {
	x := 0
	fmt.Println("x befor", &x)
	fmt.Println("x befor", x)
	foo(&x)
	fmt.Println("x after", &x)
	fmt.Println("x after", x)
}

func foo(y *int) {
	fmt.Println("y befor", y)
	fmt.Println("y befor", *y)
	*y = 43
	fmt.Println("y after", y)
	fmt.Println("y after", *y)
}

func pointer2() {
	x := 45
	fmt.Println(&x)
	s := &x
	fmt.Println("Value of s is : ", s)
	fmt.Println("address of s is : ", &s)
	fmt.Println("value of the address of s is : ", *s)
	y := 33
	fmt.Println("address of y is : ", &y)
	s = &y
	fmt.Println("value of the address of s is : ", *s)
	fmt.Println("value of x is : ", x)
	*s = y
	fmt.Println("Value of s is : ", s)
	fmt.Println("value of x is : ", s)
	fmt.Println("address of s is : ", &s)
	// x wont change because s has always been a new variable
	fmt.Println("value of x is : ", x)
}

//type Person struct {
//}
//// value of type pointer to person
//func changeME(per *Person)  {
//
//}