The Go programming language has a highly matured standard library that provides tools for collecting runtime metrics and generating application profiles. Currently, the following profile types are supported.
### heap: 
A sampling of memory allocations of live objects. You can specify the gc GET parameter to run GC before taking the heap sample.
### profile: 
The CPU profile. It is a type of application profile that captures the amount of CPU time a program spends at each trace point. When CPU profiling is enabled, the runtime will interrupt itself every 10 milliseconds and record the stack traces of currently running go-routines.
### block: 
Stack traces that led to blocking on synchronization primitives
### allocs: 
A sampling of all past memory allocations
### cmdline: 
The command line invocation of the current program
### goroutine: 
Stack traces of all current go-routines
### mutex: 
Stack traces of holders of contended mutexes
### threadcreate: 
Stack traces that led to the creation of new OS threads

## view the number of coroutines
```shell
go tool pprof http://localhost:8081/debug/pprof/goroutine
```
After the command runs, enter web in the console and press Enter.
View specific data list in web browser
### View specific data list in web browser
```shell
http://localhost:8081/debug/pprof/goroutine?debug=1
```
