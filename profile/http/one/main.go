package main

import (
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"time"
)

func main() {

	// start the profiling server in a seprate go routine so it doesnt block
	go func() {
		err := http.ListenAndServe(":8080", nil)
		if err != nil {
			return
		}
	}()
	words := []string{"sky", "forest", "fly", "cup", "wood",
		"falcon", "so", "see", "tool"}
	filtered := Filter(words)
	go func() {
		for i := 0; i < 5; i++ {
			_ = Filter(words)
			time.Sleep(3 * time.Second)
		}
	}()
	fmt.Println(filtered)
	for {
		f := Fib(60)
		fmt.Println(fmt.Sprintf("Fibonacci number is: %d", f))
	}
}

func Fib(n int) int {
	if n < 2 {
		return n
	}
	return Fib(n-1) + Fib(n-2)
}

func Filter(words []string) []string {
	filtered := []string{}
	for i := range words {
		if len(words[i]) == 3 {
			filtered = append(filtered, words[i])
		}
	}
	return filtered
}
