package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

func main() {
	start := time.Now()
	result := make(chan string)
	urls := []string{
		"https://golang.org",
		"https://godoc.org",
		"https://play.golang.org",
		"http://gopl.io",
		"http://gopl.io",
		"https://golang.org",
		"https://godoc.org",
		"https://play.golang.org",
		"http://gopl.io",
		"http://gopl.io",
		"https://godoc.org",
	}
	for _, url := range urls {
		go fetch(url, result)
	}
	for range urls {
		fmt.Println(<-result)
	}

	fmt.Printf("%.2fs   elapsed\n", time.Since(start).Seconds())
}

func fetch(url string, result chan<- string) {
	start := time.Now()

	resp, err := http.Get(url)
	if err != nil {
		result <- err.Error()
		return
	}
	nbytes, err := io.Copy(ioutil.Discard, resp.Body)
	resp.Body.Close() // don't leak resources
	if err != nil {
		result <- fmt.Sprintf("while reading %s: %v", url, err)
		return
	}
	secs := time.Since(start).Seconds()
	result <- fmt.Sprintf("%.2fs   %7d  %s", secs, nbytes, url)
}
