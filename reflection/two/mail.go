
// Golang program to illustrate 
// reflect.MethodByName() Function 

package main

import (
	"fmt"
	"reflect"
)

// Main function 
type T struct {}

func (t *T) GFG() {
	fmt.Println("I am Awesome")
}
type YourT2 struct {}
func (y YourT2) MethodFoo(i int, oo string) {
	fmt.Println(i)
	fmt.Println(oo)
}

func Invoke(any interface{}, name string, args... interface{}) {
	inputs := make([]reflect.Value, len(args))
	for i, _ := range args {
		inputs[i] = reflect.ValueOf(args[i])
	}
	reflect.ValueOf(any).MethodByName(name).Call(inputs)
}

func main() {
	var t T
	reflect.ValueOf(&t).MethodByName("GFG").Call([]reflect.Value{})
	Invoke(YourT2{}, "MethodFoo", 10, "Geekforgeeks")
}