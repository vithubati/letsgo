package main

import (
	"errors"
	"fmt"
	"log"
	"reflect"
)

type order struct {
	ordId      int
	customerId int
}

type employee struct {
	name    string
	id      int
	address string
	salary  int
	country string
}

func createQuery(q interface{}) (string, error) {
	if reflect.ValueOf(q).Kind() == reflect.Struct {
		t := reflect.TypeOf(q).Name()
		query := fmt.Sprintf("insert into %s values(", t)
		v := reflect.ValueOf(q)
		for i := 0; i < v.NumField(); i++ {
			switch v.Field(i).Kind() {
			case reflect.Int:
				if i == 0 {
					query = fmt.Sprintf("%s%d", query, v.Field(i).Int())
				} else {
					query = fmt.Sprintf("%s, %d", query, v.Field(i).Int())
				}
			case reflect.String:
				if i == 0 {
					query = fmt.Sprintf("%s\"%s\"", query, v.Field(i).String())
				} else {
					query = fmt.Sprintf("%s, \"%s\"", query, v.Field(i).String())
				}
			default:
				return "", errors.New(fmt.Sprintf("Struct has an Unsupported type"))
			}
		}
		query = fmt.Sprintf("%s)", query)
		return query, nil
	}
	return "", errors.New(fmt.Sprintf("unsupported type"))
}

func main() {
	o := order{
		ordId:      456,
		customerId: 56,
	}
	query, err := createQuery(o)
	if err != nil {
		log.Println(err)
	}
	fmt.Println(query)

	e := employee{
		name:    "Naveen",
		id:      565,
		address: "Coimbatore",
		salary:  90000,
		country: "India",
	}
	query, err = createQuery(e)
	if err != nil {
		log.Println(err)
	}
	fmt.Println(query)
	i := 90
	query, err = createQuery(i)
	if err != nil {
		log.Println(err)
	}
	fmt.Println(query)
}
