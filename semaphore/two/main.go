package main

import (
	"context"
	"fmt"
	"golang.org/x/sync/semaphore"
	"time"
)

// remoteDeleteEmployeeRPC will delete an employee over the network.
func remoteDeleteEmployeeRPC(id int) {
	fmt.Println("starting for: ", id)
	time.Sleep(4 * time.Second)
	fmt.Println("Done remoteDeleteEmployeeRPC for: ", id)
}

// sem is a weighted semaphore allowing up to 10 concurrent operations.
var sem = semaphore.NewWeighted(int64(4))

func main() {
	// a context is required for the weighted semaphore pkg.
	ctx := context.Background()
	employeeList := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	for _, employee := range employeeList {
		if err := sem.Acquire(ctx, 1); err != nil {
			// handle error and maybe break
		}
		go func(id int){
			remoteDeleteEmployeeRPC(id)
			sem.Release(1)
		}(employee)
	}

	time.Sleep(14 * time.Second)
}
