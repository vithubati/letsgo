package main

import (
	"fmt"
	"time"
)

// remoteDeleteEmployeeRPC will delete an employee over the network.
func remoteDeleteEmployeeRPC(id int) {
	fmt.Println("starting for: ", id)
	time.Sleep(4 * time.Second)
	fmt.Println("Done remoteDeleteEmployeeRPC for: ", id)
}

// sem is a channel that will allow up to 10 concurrent operations.
var sem = make(chan int, 3)

func main() {
	employeeList := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	for _, employee := range employeeList {
		sem <- 1
		go func(id int) {
			remoteDeleteEmployeeRPC(id)
			<-sem
		}(employee)
	}

	time.Sleep(14 * time.Second)
}
