package stack

import "fmt"

type stringStack struct {
	stack []string
}

func StringStack() *stringStack {
	return &stringStack{
		stack: make([]string, 0),
	}
}

func (s *stringStack) Push(item string) {
	s.stack = append(s.stack, item)
}

func (s *stringStack) IsEmpty() bool {
	return s.Size() == 0
}

func (s *stringStack) Size() int {
	return len(s.stack)
}

func (s *stringStack) Pop() (string, bool) {
	if s.IsEmpty() {
		return "", false
	}
	last, _ := s.Peek()
	s.stack = s.stack[:s.Size()-1]
	return last, true
}

func (s *stringStack) Peek() (string, bool) {
	if s.IsEmpty() {
		return "", false
	}
	return s.stack[s.Size()-1], true
}

func (s *stringStack) String() string {
	return fmt.Sprint(s.stack)
}
