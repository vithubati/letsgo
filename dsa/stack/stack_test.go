package stack

import (
	"fmt"
	"testing"
)

func Test_stringStack_Push(t *testing.T) {
	stack := StringStack()
	stack.Push("Vithu")
	stack.Push("Bruce")
	stack.Push("Vino")
	stack.Push("Tina")
	stack.Push("Bati")
	fmt.Println(stack.String())
	stack.Pop()
	stack.Pop()
	stack.Pop()
	fmt.Println(stack.String())
	last, _:= stack.Peek()
	fmt.Println(last)
	fmt.Println(stack.Size())
	fmt.Println(stack.IsEmpty())
	stack.Pop()
	stack.Pop()
	fmt.Println(stack.IsEmpty())
}
