package stack

import "fmt"

type byteStack struct {
	stack []byte
}

func ByteStack() *byteStack {
	return &byteStack{
		stack: make([]byte, 0),
	}
}

func (s *byteStack) Push(item byte) {
	s.stack = append(s.stack, item)
}

func (s *byteStack) IsEmpty() bool {
	return s.Size() == 0
}

func (s *byteStack) Size() int {
	return len(s.stack)
}

func (s *byteStack) Pop() ([]byte, bool) {
	if s.IsEmpty() {
		return nil, false
	}
	s.stack = s.stack[:s.Size()-1]
	return s.stack, true
}

func (s *byteStack) Peek() (byte, bool) {
	if s.IsEmpty() {
		return 0, false
	}
	return s.stack[s.Size()-1], true
}

func (s *byteStack) String() string {
	return fmt.Sprint(s.stack)
}
