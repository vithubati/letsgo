package array

import (
	"errors"
	"fmt"
	"strings"
)

type array struct {
	lastIndex int
	bucket    []int
	cap       int
}

func New(cap int) *array {
	return &array{
		bucket:    make([]int, cap, cap),
		lastIndex: -1,
		cap:       cap,
	}
}

func (a *array) Length() int {
	return a.lastIndex + 1
}

func (a *array) IndexOf(item int) int {
	for i := 0; i < a.lastIndex+1; i++ {
		if a.bucket[i] == item {
			return i
		}
	}
	return -1
}

func (a *array) ItemAt(index int) (int, error) {
	if index < 0 || index > a.lastIndex {
		return -1, errors.New("invalid index")
	}
	return a.bucket[index], nil
}

func (a *array) Insert(item int) {
	if a.lastIndex == cap(a.bucket)-1 {
		newBucket := make([]int, len(a.bucket)*2)
		copy(newBucket, a.bucket)
		a.bucket = newBucket
	}
	a.lastIndex++
	a.bucket[a.lastIndex] = item
	fmt.Println(a.String())
}

func (a *array) RemoveAt(index int) error {
	if index < 0 || index > a.lastIndex {
		return errors.New("invalid index")
	}
	for i := index; i < a.lastIndex; i++ {
		a.bucket[i] = a.bucket[i+1]
	}
	a.lastIndex--
	fmt.Println(a.String())
	return nil
}

func (a *array) String() string {
	var builder strings.Builder
	builder.WriteString("[")
	for i := 0; i < a.lastIndex+1; i++ {
		builder.WriteString(fmt.Sprint(a.bucket[i]))
		if i != a.lastIndex {
			builder.WriteString(",")
		}
	}
	builder.WriteString("]")
	return builder.String()
	//return fmt.Sprint(a.bucket)
}
