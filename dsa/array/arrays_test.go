package array

import (
	"fmt"
	"testing"
)

func TestArray_Insert(t *testing.T) {
	arr := New(2)
	println(arr.Length())
	fmt.Println(arr.String())
	arr.Insert(5)
	arr.Insert(3)
	arr.Insert(11)
	arr.Insert(12)
	println(arr.Length())
	if err:= arr.RemoveAt(3); err != nil {
		fmt.Println(err.Error())
	}
	println(arr.Length())
	arr.Insert(22)
	println(arr.Length())
	arr.Insert(24)
	idx, err:= arr.ItemAt(4)
	if err != nil {
		fmt.Println(err.Error())
	}
	println(idx)
	println(arr.Length())
	//arr.Insert(12)
	//arr.RemoveAt(1)
	//fmt.Println(arr.String())
	//arr.RemoveAt(2)
	//fmt.Println(arr.String())
	//arr.RemoveAt(3)
	//fmt.Println(arr.String())
}

