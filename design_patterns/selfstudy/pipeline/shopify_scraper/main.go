package main

import (
	"fmt"
	"github.com/google/uuid"
	"time"
)

type App struct {
	Rating   float64
	Review   int
	Name     string
	Category []string
	AppLink  string
	PageLink string
}

type ShopifyScraper struct{}

// Generate - Generation Stage, This stage will generate app directory URLs.
func (s *ShopifyScraper) Generate(done <-chan bool) chan App {

	links := make(chan App)
	go func() {
		defer close(links)
		for i := 0; i < 5; i++ {
			select {
			case <-done:
				return
			case links <- App{PageLink: fmt.Sprintf("https://apps.shopify.com/browse/all?page=%d", i)}:
			}
		}
	}()
	return links
}

// AppDirectoryScraper - Scraper Stage, This stage will visit directory page and
// scrape different app URLs from the page for further processing.
func (s *ShopifyScraper) AppDirectoryScraper(done <-chan bool, input <-chan App) chan App {

	apps := make(chan App)
	go func() {
		defer close(apps)
		for app := range input {

			// Extract Different App URLs by scraping  using app.PageLink
			time.Sleep(1 *time.Second)
			app.AppLink = uuid.New().String()
			select {
			case <-done:
				return
			case apps <- app:
			}
		}
	}()
	return apps
}

// AppScraper - App Scraper Stage, This stage will visit app page and scrape app information
func (s *ShopifyScraper) AppScraper(done <-chan bool, input <-chan App) chan App {
	apps := make(chan App)
	go func() {
		defer close(apps)
		for app := range input {

			// Extract Different App Information by scraping by using app.AppLink
			time.Sleep(1 *time.Second)
			app.Name = app.AppLink
			select {
			case <-done:
				return
			case apps <- app:

			}
		}
	}()
	return apps
}

func (s *ShopifyScraper) Do() []App {
	var result []App
	done := make(chan bool)
	defer close(done)

	for i := range s.AppScraper(done, s.AppDirectoryScraper(done, s.Generate(done))) {
		result = append(result, i)
	}
	return result
}

func main() {
	s:= ShopifyScraper{}
	apps:= s.Do()
	for _, app := range apps {
		fmt.Println(fmt.Sprintf("%+v", app))
	}

	fmt.Println("done")
}
