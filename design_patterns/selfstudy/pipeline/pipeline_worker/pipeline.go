// Package pipeline_worker pipeline_worker https://ketansingh.me/posts/pipeline-pattern-in-go-part-2/
package pipeline_worker

//Message represents individual messages which stages will be passing among themselves.
type Message interface {
}

//Stage represents different stages which process a Message and can produce one or more message.
type Stage interface {
	Process(stage Message) ([]Message, error)
}

type PipelineOpts struct {
	Concurrency int
}
type Pipeline interface {

	// AddPipe will add a Stage and link them in same order
	AddPipe(pipe Stage, opt *PipelineOpts)

	// Start will start the Pipeline
	Start() error

	// Stop will stop the Pipeline and will also wait until existing messages get processed
	Stop() error

	// Input will be start ends of pipeline
	Input() chan<- Message

	// Output will be finish ends of pipeline
	Output() <-chan Message
}
