package main

import (
	pipeline_worker2 "bitbucket.org/vithubati/letsgo/design_patterns/selfstudy/pipeline/pipeline_worker"
	"fmt"
	"github.com/google/uuid"
	"log"
	"time"
)

type MultiplyHundredSlow struct{}

func (m MultiplyHundredSlow) Process(result pipeline_worker2.Message) ([]pipeline_worker2.Message, error) {
	number := result.(Process)
	fmt.Println("(m MultiplyHundredSlow) Process is: ", number.Num)
	time.Sleep(10 * time.Second)
	return []pipeline_worker2.Message{number.Num * 100, number.Num * 100}, nil
}

type MultiplyTenSlow struct{}

func (m MultiplyTenSlow) Process(result pipeline_worker2.Message) ([]pipeline_worker2.Message, error) {
	number := result.(Process)
	fmt.Println("(m MultiplyTenSlow) Process is: ", number)
	time.Sleep(2 * time.Second)
	//return []pipeline_worker.Message{number * 10, number * 4, number * 2, number * 1}, nil // this also possible
	number.Num = number.Num * 10
	return []pipeline_worker2.Message{number}, nil
}

type DivideTwoSlow struct{}

func (m DivideTwoSlow) Process(result pipeline_worker2.Message) ([]pipeline_worker2.Message, error) {
	number := result.(Process)
	fmt.Println("(m DivideTwoSlow) Process is: ", number)
	time.Sleep(5 * time.Second)
	number.Num = number.Num / 2
	return []pipeline_worker2.Message{number}, nil
}

type Process struct {
	ID  string
	Num int
}

// ONLY caveat  of this design is that its not promised that the inputs will be processed in order
func main() {

	p := pipeline_worker2.NewConcurrentPipeline()

	//p.AddPipe(MultiplyHundredSlow{}, &pipeline_worker.PipelineOpts{
	//	Concurrency: 1,
	//})
	p.AddPipe(MultiplyTenSlow{}, &pipeline_worker2.PipelineOpts{
		Concurrency: 2,
	})
	p.AddPipe(DivideTwoSlow{}, &pipeline_worker2.PipelineOpts{
		Concurrency: 3,
	})

	if err := p.Start(); err != nil {
		log.Println(err)
	}
	nums := []int{2, 4, 8}
	for _, num := range nums {
		ps := Process{
			ID:  uuid.New().String(),
			Num: num,
		}
		fmt.Println("inputting: ", ps)
		p.Input() <- ps
	}

	count := 0
	go func() {
		for number := range p.Output() {
			fmt.Println("End Output is: ", number)
			count++
		}
	}()

	err := p.Stop()
	if err != nil {
		fmt.Println("error", err.Error())
	}
	fmt.Println("Done? ")
	time.Sleep(20 * time.Second)
	fmt.Println("count is: ", count)

}
