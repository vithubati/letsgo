package pipeline_worker

import "errors"

var ErrConcurrentPipelineEmpty = errors.New("concurrent pipeline empty")

type ConcurrentPipeline struct {
	workerGroups []StageWorker
}

// AddPipe will add a Stage and link them in same order
func (c *ConcurrentPipeline) AddPipe(pipe Stage, opt *PipelineOpts) {

	if opt == nil {
		opt = &PipelineOpts{Concurrency: 1}
	}

	var input = make(chan Message, 10)
	var output = make(chan Message, 10)

	for _, i := range c.workerGroups {
		input = i.Output()
	}

	worker := NewWorkerGroup(opt.Concurrency, pipe, input, output)
	c.workerGroups = append(c.workerGroups, worker)
}

// Output will be finish ends of pipeline
func (c *ConcurrentPipeline) Output() <-chan Message {
	sz := len(c.workerGroups)
	return c.workerGroups[sz-1].Output()
}

// Input will be start ends of pipeline
func (c *ConcurrentPipeline) Input() chan<- Message {
	return c.workerGroups[0].Input()
}

// Start will start the Pipeline
func (c *ConcurrentPipeline) Start() error {

	if len(c.workerGroups) == 0 {
		return ErrConcurrentPipelineEmpty
	}

	for i := 0; i < len(c.workerGroups); i++ {
		g := c.workerGroups[i]
		g.Start()
	}

	return nil
}

// Stop will stop the Pipeline and will also wait until existing messages get processed
func (c *ConcurrentPipeline) Stop() error {

	for _, i := range c.workerGroups {
		close(i.Input())
		err := i.WaitStop()
		if err != nil {
			return err
		}
	}

	sz := len(c.workerGroups)
	close(c.workerGroups[sz-1].Output())
	return nil
}

func NewConcurrentPipeline() Pipeline {
	return &ConcurrentPipeline{}
}
