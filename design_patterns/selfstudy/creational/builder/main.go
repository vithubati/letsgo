package main

import (
	"log"
)

type Speed float64

const (
	MPH Speed = 1
	KPH       = 1.60934
)

type Color string

const (
	BlueColor  Color = "blue"
	GreenColor       = "green"
	RedColor         = "red"
)

type Wheels string

const (
	SportsWheels Wheels = "sports"
	SteelWheels         = "steel"
)

type Builder interface {
	Paint(Color) Builder
	Wheels(Wheels) Builder
	TopSpeed(Speed) Builder
	Build() Car
}

type Car interface {
	Drive() error
	Stop() error
}

// the implementation of Builder interface
type carBuilder struct {
	color    Color
	wheels   Wheels
	topSpeed Speed
}

func (b carBuilder) Paint(color Color) Builder {
	b.color = color
	return b
}

func (b carBuilder) Wheels(wheels Wheels) Builder {
	b.wheels = wheels
	return b
}

func (b carBuilder) TopSpeed(speed Speed) Builder {
	b.topSpeed = speed
	return b
}

func (b carBuilder) Build() Car {
	//return &carBuilderIf{}
	switch b.wheels {
	case SportsWheels:
		return SportsCar{}
	case SteelWheels:
		return FamilyCar{}
	}
	return nil
}

// the implementation of Interface interface
type FamilyCar struct{

}

func (f FamilyCar) Drive() error {
	log.Println("FamilyCar Drive()")
	return nil
}
func (f FamilyCar) Stop() error {
	log.Println("FamilyCar Stop()")
	return nil
}

// the implementation of Interface interface
type SportsCar struct{}

func (s SportsCar) Drive() error {
	log.Println("SportsCar Drive()")
	return nil
}

func (s SportsCar) Stop() error {
	log.Println("SportsCar Stop()")
	return nil
}

func NewBuilder(builder string) Builder {
	switch builder {
	case "car":
		return carBuilder{}
	default:
		return carBuilder{}
	}
}

func main() {
	// look how Builder pattern separates the construction of a complex object from its representation.
	// so that the same construction process can create different representations.
	assembly := NewBuilder("car")

	familyCar := assembly.Paint(BlueColor).Wheels(SteelWheels).TopSpeed(50 * MPH).Build()
	familyCar.Drive()

	sportsCar := assembly.Paint(RedColor).Wheels(SportsWheels).TopSpeed(150 * MPH).Build()
	sportsCar.Drive()
}
