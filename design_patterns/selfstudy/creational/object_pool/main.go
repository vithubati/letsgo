package main

type DbConnection struct {}


type Pool chan *DbConnection

func (r *Pool) Do()  {
}

func New(total int) *Pool {
	p := make(Pool, total)

	for i := 0; i < total; i++ {
		p <- new(DbConnection)
	}

	return &p
}

func main() {
	//p := New(2)

	//select {
	//case obj := <-p:
	//	obj.Do( )
	//
	//	p <- obj
	//default:
	//	// No more objects left — retry later or fail
	//	return
	//}

}