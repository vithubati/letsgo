package main

import (
	"fmt"
	"sync"
	"time"
)

// singleton is private struct, it should be created and fetched by GetSingletonObject func
type singleton struct {
	name string
}

func (s *singleton) setName(name string) {
	fmt.Println("address of s ==========",  &s.name)
	s.name = name
}

func (s *singleton) getName() string{
	fmt.Println("address of s ==========",  &s.name)
	return s.name
}

var (
	once     sync.Once
	instance *singleton
)

func GetSingletonObject() *singleton {
	once.Do(func() {
		instance = new(singleton)
		instance.setName("jojo")
		fmt.Println("hhhhiiiii")
	})
	fmt.Println("returing ....")
	return instance
}
func main() {
	var one = GetSingletonObject()
	time.Sleep(5*time.Second)
	one.setName("vithu")
	time.Sleep(5*time.Second)
	var two = GetSingletonObject()
	fmt.Println(two.getName())
}

