package main

import (
	"errors"
	"fmt"
)

type Payment interface {
	Pay(amount float32) string
}

const (
	Cash      = 1
	DebitCard = 2
)

type CashPayment struct{}

func (c *CashPayment) Pay(amount float32) string {
	return fmt.Sprintf("%v paid using cash\n", amount)
}

type DebitCardPayment struct{}

func (c *DebitCardPayment) Pay(amount float32) string {
	return fmt.Sprintf("%v paid using DebitCard\n", amount)
}

func GetPayment(m int) (Payment, error) {
	switch m {
	case Cash:
		return new(CashPayment), nil
	case DebitCard:
		return new(DebitCardPayment), nil
	default:
		return nil, errors.New(fmt.Sprintf("Payment method %d was not recognized", m))
	}
}

func main() {
	paymentMethod, err := GetPayment(Cash)
	if err != nil {
		panic(fmt.Sprintf("Error %s", err.Error()))
	}
	fmt.Println(paymentMethod.Pay(32))
}
