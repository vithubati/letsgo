//Decouples an interface from its implementation so that the two can vary independently
package main

import "fmt"

// abstraction
type Vehicle interface {
	Run()
	MaxSpeed() int
}

// implementor
type Driver interface {
	Drive(vi Vehicle)
}

// refined abstraction
type Truck struct {
	Driver   Driver
	maxSpeed int
}

func (t Truck) Run() {
	println("truck begin to move...")
	t.Driver.Drive(t)
}

func (t Truck) MaxSpeed() int {
	return t.maxSpeed
}

// refined abstraction
type Car struct {
	Driver   Driver
	maxSpeed int
}

func (c Car) Run() {
	println("car begin to move...")
	c.Driver.Drive(c)
}

func (c Car) MaxSpeed() int {
	return c.maxSpeed
}

// concrete implementor
type RookieDriver struct{}

func (RookieDriver) Drive(vi Vehicle) {
	println("rookie driver -> speed", vi.MaxSpeed()>>1, "km/h")
}

// concrete implementor
type OldDriver struct{}

func (OldDriver) Drive(vi Vehicle) {
	println("old driver -> speed", vi.MaxSpeed(), "km/h")
}

// The bridge mode and the strategy mode have many similarities, because they both hand over the behavior to another interface implementation,
// Bridging solves the combination of callers and behaviors such as A+M, B+N, A+N, etc. It combines the unmatched interfaces
// so that they can be called uniformly. Once the combination is running, it is generally Will not change.
// The strategy mode is to dynamically expand and change the algorithm.
// In essence, bridging is a structural mode and strategy is a behavioral mode, which focuses on the combination of structures
//and the communication between structures.
func main() {
	old := OldDriver{}
	rk := RookieDriver{}
	v1 := Truck{old, 60}
	v2 := Car{old, 130}
	v3 := Truck{rk, 60}
	v4 := Car{rk, 130}
	list := []Vehicle{v1, v2, v3, v4}
	for _, v := range list {
		v.Run()
	}
	/*
		output:
		truck begin to move...
		old driver -> speed 60 km/h
		car begin to move...
		old driver -> speed 130 km/h
		truck begin to move...
		rookie driver -> speed 30 km/h
		car begin to move...
		rookie driver -> speed 65 km/h
	*/
	names := [3]string{"vithu", "Vinoth", "Bruce"}
	fmt.Println(names)
	change(names)
	fmt.Println(names)
}

func change(nm [3]string)  {
	nm[2] = "Bati"
}