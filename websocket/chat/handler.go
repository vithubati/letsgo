package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
)

// serveWs handles websocket requests from the peer.
func serveWs(w http.ResponseWriter, r *http.Request) {
	var hub *Hub
	c := r.Header.Get("Sec-WebSocket-Protocol")
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	i, err := strconv.Atoi(c)
	if err != nil {
		log.Println(err)
		return
	}
	if h, ok := hubs[Colors[i]]; ok {
		hub = h
	} else {
		hub = newHub(i)
		hubs[hub.Color] = hub
	}

	go hub.run()
	fmt.Println("Creating a Client")
	client := &Client{
		hub:  hub,
		conn: conn,
		send: make(chan []byte, 256),
		name: randomString(5),
	}
	fmt.Println(hub.register)
	fmt.Println(client.hub.register)
	hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}
