package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"math/rand"
	"time"
)

var hubs = make(map[string]*Hub, 0)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type Message struct {
	Name    string `json:"name"`
	ID      int    `json:"id"`
	Color   string `json:"color"`
	Message string `json:"message"`
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	name string
	hub  *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	fmt.Println("readPump starting")
	defer func() {
		fmt.Println("Un registering----------------------- ", c.name)
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		fmt.Println("waiting to Read next Message. from ", c.name)
		_, message, err := c.conn.ReadMessage()
		fmt.Println("Read a Message from", c.name)
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			fmt.Println("Going to break read loop.", err.Error())
			break
		}
		fmt.Println("Going to broadcast read message. from ", c.name)
		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		if string(message) == "bad" {
			continue
		}
		msg := &Message{
			Name:    c.name,
			ID:      int(rand.Uint32()),
			Color:   c.hub.Color,
			Message: string(message),
		}
		b, err := json.Marshal(msg)
		if err != nil {
			return
		}
		c.hub.broadcast <- b
	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	fmt.Println("writePump starting")
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		fmt.Println("writePump------------conn.Close----------- ", c.name)
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			//time.Sleep(5 * time.Second)
			fmt.Println("Going to write to a client. ", c.name)
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				fmt.Println("The hub closed the channel.", c.name)
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			// Add queued chat messages to the current websocket message.
			n := len(c.send)
			for i := 0; i < n; i++ {
				fmt.Println("writing to a client next writer.", c.name)
				w.Write(newline)

				w.Write(<-c.send)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			fmt.Println("<-ticker.C. SetWriteDeadline")
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}
