package main

import (
	"bytes"
	"fmt"
)

func reverse(input string) string {
	var builder []byte

	for _, s := range []byte(input) {
		builder = bytes.Join([][]byte{{s}, builder}, []byte{})
	}
	return string(builder)
}

func main() {
	out := reverse("Vithu")
	fmt.Println(out)
	input := "Hola"
	outs := bytes.ReplaceAll([]byte(input), []byte("H"), []byte("B"))
	fmt.Println(string(outs))
	idx := bytes.Index([]byte(input), []byte("h"))
	//idx := bytes.
	fmt.Println(idx)
}
