package main

import (
	"bufio"
	"embed"
	"fmt"
	"io/ioutil"
	"os"
)

//go:embed planets.txt
var pro embed.FS

func main() {
	file, err := pro.Open("planets.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()
	reader := bufio.NewReader(file)
	//
	//for {
	//	line, err := reader.ReadString('\n')
	//	if err != nil {
	//		if err == io.EOF {
	//			break
	//		} else {
	//			fmt.Println(err)
	//			os.Exit(1)
	//		}
	//	}
	//	fmt.Print(line)
	//}
	bytes, err := ioutil.ReadAll(reader)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Printf("%s", bytes)
}
