package main

import (
	"fmt"
	"io"
	"os"
	"strings"
)


func main() {
	str:= "Clear is better than clever"
	//fmt.Println(len(str))
	//fmt.Println(str[0])
	//fmt.Println(str[1])
	reader := strings.NewReader(str)
	buf := make([]byte, 3)
	for {
		n, err := reader.Read(buf)
		//fmt.Println(n)
		if err != nil{
			if err == io.EOF {
				fmt.Println("hi:", buf)
				fmt.Println(string(buf[:n])) //should handle any remaining bytes.
				break
			}
			fmt.Println(err)
			os.Exit(1)
		}
		fmt.Println(string(buf))
	}
}
