package main

import (
	"embed"
	"fmt"
	"io"
	"os"
)

//go:embed proverbs.txt
var pro embed.FS

func main() {
	file, err := pro.Open("proverbs.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	if _, err := io.Copy(os.Stdout, file); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	os.Stdout.Write([]byte(`vithu`))
}
