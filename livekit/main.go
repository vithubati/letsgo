package main

import (
	"context"
	"fmt"
	"github.com/livekit/protocol/auth"
	"github.com/livekit/protocol/livekit"
	lksdk "github.com/livekit/server-sdk-go"
	"log"
	"time"
)

func getJoinToken(apiKey, apiSecret, room, identity string) (string, error) {
	at := auth.NewAccessToken(apiKey, apiSecret)
	t := true
	grant := &auth.VideoGrant{
		RoomJoin:       true,
		Room:           room,
		CanPublish:     &t,
		CanSubscribe:   &t,
		CanPublishData: &t,
	}
	at.AddGrant(grant).
		SetIdentity(identity).
		SetValidFor(288 * time.Hour)

	return at.ToJWT()
}

func main() {
	host := "https://streaam.feenixtek.com"
	apiKey := "qriosapikey"
	apiSecret := "blabla"

	roomName := "TestRoom"

	roomClient := lksdk.NewRoomServiceClient(host, apiKey, apiSecret)

	// create a new room
	room, err := roomClient.CreateRoom(context.Background(), &livekit.CreateRoomRequest{
		Name: roomName,
	})
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(fmt.Sprintf("room: %+v", room))
	token, err := getJoinToken(apiKey, apiSecret, room.Name, "Vithu")

	fmt.Println(fmt.Sprintf("token: %+v", token))
	// list rooms
	res, err := roomClient.ListRooms(context.Background(), &livekit.ListRoomsRequest{})
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(fmt.Sprintf("room: %+v", res))
}
