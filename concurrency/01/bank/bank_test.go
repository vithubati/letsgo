package bank_test

import (
	"bitbucket.org/vithubati/letsgo/concurrency/01/bank"
	"fmt"
	"testing"
)

type Done struct {
	Name  string
	State string
}

func TestBank(t *testing.T) {
	done := make(chan Done)

	// Alice
	go func(d chan Done) {
		bank.Deposit(200, "Alice")
		fmt.Println("=", bank.Balance())
		d <- Done{Name: "Alice", State: "Done"}
	}(done)

	// Bob
	go func(d chan Done) {
		bank.Deposit(100, "Bob")
		d <- Done{Name: "Bob", State: "Done"}
	}(done)

	// Wait for both transactions.
	fmt.Println(<-done)
	fmt.Println(<-done)

	if got, want := bank.Balance(), 300; got != want {
		t.Errorf("Balance = %d, want %d", got, want)
	}
}
