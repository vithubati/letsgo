package main

import (
	"bitbucket.org/vithubati/letsgo/concurrency/01/bank"
	"fmt"
	"time"
)

func main() {

	go func() {
		time.Sleep(3 * time.Second)
		bank.Deposit(200, "B") // A1
	}()
	go func() {
		time.Sleep(5 * time.Second)
		bank.Deposit(200, "C") // A1
	}()
	go func() {
		time.Sleep(10 * time.Second)
		bank.Deposit(50, "D") // A1
	}()

	// Bob:
	go bank.Deposit(100, "A") // B
	for {
		time.Sleep(2 * time.Second)
		fmt.Println("=", bank.Balance()) // A2
	}
}
