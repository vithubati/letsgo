package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer func() {
		fmt.Println("Cancelled!", ctx.Err())
	}()

	for n := range gen(ctx) {
		fmt.Println("reading.....")
		fmt.Println(n)
		if n == 5 {
			fmt.Println("Cancelling.....")
			cancel()  // cancel when we are finished
			break
		}
	}
}

func gen(ctx context.Context) <-chan int {
	dst := make(chan int)
	n := 1
	go func() {
		for {
			select {
			case <-ctx.Done():
				return // returning not to leak the goroutine
			case dst <- n:
				fmt.Println("adding.....")
				time.Sleep(5*time.Second)
				n++
			}
		}
	}()
	return dst
}
