package main

import (
	"context"
	"fmt"
)

// context keys

type Session struct {
	token string
	userId int
}

type contextKey string
func (c contextKey) String() string {
	return "mypackage context key " + string(c)
}
var (
	contextKeySession = contextKey("session")
	contextKeyRole   = contextKey("role")
)
// sessionFromContext gets the auth session from the context.
func sessionFromContext(ctx context.Context) (Session, bool) {
	session, ok := ctx.Value(contextKeySession).(Session)
	return session, ok
}

// roleFromContext gets the auth session from the context.
func roleFromContext(ctx context.Context) (string, bool) {
	role, ok := ctx.Value(contextKeyRole).(string)
	return role, ok
}


func main() {
	session := Session{
		token: "444-GGG-333-DDD",
		userId: 1255,
	}
	ctx := context.WithValue(context.Background(), contextKeySession, session)
	ctx = context.WithValue(ctx, contextKeyRole, "i am awesome")
	tok, ok := sessionFromContext(ctx)
	if !ok {
		fmt.Println("no session found")
	}
	fmt.Println(tok)
	role, ok := roleFromContext(ctx)
	if !ok {
		fmt.Println("no role found")
	}
	fmt.Println(role)
}
