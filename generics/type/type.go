package main

import "fmt"

type StringableBunch[E fmt.Stringer] []E

type Bunch[E any] []E

func (b Bunch[E]) Print() {

}

func PrintBunch[E any](b Bunch[E]) {
	b.Print()
}

func main() {
	x := Bunch[int]{1, 2, 3}
	println(x)
}
