package main

import (
	"fmt"
	"golang.org/x/exp/constraints"
)

// TreeNode A node in a binary tree which you can search by key
type TreeNode[K constraints.Ordered, V any] struct {
	Key         K
	Value       V
	left, right *TreeNode[K, V]
}

// String Give string representation of node in print statements
func (tn *TreeNode[K, V]) String() string {
	return fmt.Sprintf("TreeNode(%v, %v)", tn.Key, tn.Value)
}

// Insert node n into a leaf underneath parent node.
// Position will be determined based on value of key
func (tn *TreeNode[K, V]) Insert(n *TreeNode[K, V]) {
	if n.Key >= tn.Key {
		if tn.right == nil {
			tn.right = n
		} else {
			tn.right.Insert(n)
		}
	} else {
		if tn.left == nil {
			tn.left = n
		} else {
			tn.left.Insert(n)
		}
	}
}
func main() {

}
