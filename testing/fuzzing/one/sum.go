package one

func Sum(vals []int64) int64 {
	var total int64

	for _, val := range vals {
		if val%1e5 != 0 {
			total += val
		}
	}

	return total
}
