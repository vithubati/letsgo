package calculator

import (
	"testing"
)

func TestAdd(t *testing.T) {
	t.Parallel()
	var want float64 = 4
	got := Add(2, 2)
	if want != got {
		t.Errorf("want %f, got %f", want, got)
	}
}

func TestSubtract(t *testing.T) {
	t.Parallel()
	var want float64 = 2
	got := Subtract(4, 2)
	if want != got {
		t.Errorf("want %f, got %f", want, got)
	}
}
func BenchmarkSubtract(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = Subtract(4, 2)
	}
}
