package main

import (
	"errors"
	"fmt"
)

func main() {
	var err error
	{
		str, err := test()
		if err != nil {
			return
		}
		fmt.Println(str, err)
	}
	fmt.Println(err)
}
func test() (string, error) {
	return "str", errors.New("error")
}
