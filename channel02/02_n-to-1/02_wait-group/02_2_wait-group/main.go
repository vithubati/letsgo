package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"sync"
	"time"
)

type Node struct {
	Name string
}

func (n *Node) SetName(name string) {
	n.Name = name
}

func (n *Node) getName() string {
	return n.Name
}

func NewNodes(length int) []*Node {
	var nodes []*Node
	for i := 0; i < length; i++ {
		n := &Node{Name: "default"}
		nodes = append(nodes, n)
	}
	return nodes
}

func main() {
	go func() {
		run()
	}()
	time.Sleep(2*time.Second)
}

func run() {

	done := make(chan bool)

	var wg sync.WaitGroup
	nodes := NewNodes(5)
	for _, node := range nodes {
		wg.Add(1)
		go func(n *Node) {
			scanANode(n, &wg)
		}(node)
	}
	//fmt.Println("the address of wg is: ", &wg)
	//go func() {
	//	print(&wg)
	//
	//}()
	//
	//go func() {
	//	print(&wg)
	//}()
	//
	go func() {
		wg.Wait()
		done <- true
		close(done)
	}()

	for n := range done {
		fmt.Println(n)
	}
	fmt.Println("DONE")
}
func print(wg *sync.WaitGroup)  {
	fmt.Println("1")
	fmt.Println(fmt.Sprintf("the address of wg is: %p", wg))
	wg.Done()
}

func scanANode(node *Node, wg *sync.WaitGroup) {
	fmt.Println("----------------------------------")
	fmt.Println("===========gonna set the Name")
	SetName(node)
	fmt.Println("===========gonna get the Name")
	fmt.Println(getName(node))
	fmt.Println("----------------------------------")
	wg.Done()
}

func SetName(node *Node) {
	h:= strconv.Itoa(rand.Intn(5500))
	fmt.Println(">>>>>>>: ", h)
	node.SetName("Vithu"+ h)
}

func getName(node *Node) string{
	return node.getName()
}
