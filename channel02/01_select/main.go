package main

import (
	"fmt"
	"time"
)

func main() {
	c := make(chan int)

	go func() {
		time.Sleep(3*time.Second)
		c <- 10
	}()


	select {
	case i := <-c:
		fmt.Println(i)
	}
	fmt.Println("End of line")
}

