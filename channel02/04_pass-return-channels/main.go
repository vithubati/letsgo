package main

import (
	"fmt"
	"time"
)

func main() {
	c := incrementor()
	cSum := puller(c)
	fmt.Println("inside main returned puller. gonna range over...")
	for n := range cSum {
		fmt.Println(n)
	}
}

func incrementor() chan int {
	out := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			time.Sleep(time.Second)
			out <- i
			fmt.Println("incrementing c ...")
		}
		fmt.Println("closing incrementor out ...")
		close(out)
	}()
	fmt.Println("returning from incrementor...")
	return out
}

func puller(c chan int) chan int {
	out := make(chan int)
	go func() {
		var sum int
		for n := range c {
			time.Sleep(5*time.Second)
			sum += n
			fmt.Println("reading c ...")
		}
		out <- sum
		close(out)
		fmt.Println("closing reading out ...")
	}()
	fmt.Println("returning from puller...")
	return out
}
