package main

import (
	"fmt"
	gopdf2 "github.com/signintech/gopdf"
	"github.com/signintech/pdft"
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	pdf2()

}


func pdf1()  {
	var pt pdft.PDFt
	err := pt.Open("./test2.pdf")
	if err != nil {
		panic("Couldn't open pdf:  "+ err.Error())
	}

	err = pt.AddFont("arial", "./arial.ttf")
	if err != nil {
		log.Fatal(err)
		return
	}

	err = pt.SetFont("arial", "", 14)
	if err != nil {
		panic(err)
	}
	//insert text to pdf
	err = pt.Insert("holaaaaa aaaaaaaaa", 1, 100, 100, 100, 100, pdft.Regular)
	if err != nil {
		log.Fatal(err)
	}
	err = pt.Save("output.pdf")
	if err != nil {
		panic("Couldn't save pdf.")
	}
	fmt.Println("Done: ")
}

func pdf2() {
	var err error

	pdf := gopdf2.GoPdf{}
	pdf.Start(gopdf2.Config{PageSize: gopdf2.Rect{W: 595.28, H: 841.89}}) //595.28, 841.89 = A4

	pdf.AddPage()

	err = pdf.AddTTFFont("arial", "arial.ttf")
	if err != nil {
		panic(err)
	}

	err = pdf.SetFont("arial", "", 12)
	if err != nil {
		panic(err)
	}

	// Color the page
	pdf.SetLineWidth(0.1)
	//pdf.SetFillColor(124, 252, 0) //setup fill color
	//pdf.RectFromUpperLeftWithStyle(50, 100, 400, 600, "FD")

	pdf.SetX(1)
	pdf.SetY(1)

	// Import page 1
	tpl1 := pdf.ImportPage("test.pdf", 1, "/MediaBox")
	tpl2 := pdf.ImportPage("test2.pdf", 1, "/MediaBox")

	// Draw pdf onto page
	pdf.UseImportedTemplate(tpl1, 0, 0, 0, 0)
	pdf.Image("./sign.png", 500, 735, &gopdf2.Rect{W:60, H: 15}) //print image

	pdf.AddPage()
	pdf.UseImportedTemplate(tpl2, 0, 0, 0, 0)
	pdf.Image("./sign.png", 320, 750, &gopdf2.Rect{W:60, H: 15}) //print image
	pdf.SetX(500) //move current location
	pdf.SetY(755)
	pdf.Cell(&gopdf2.Rect{W:10, H: 15}, "20") //print text
	pdf.WritePdf("image.pdf")

}

// DownloadFile will download a url to a local file. It's efficient because it will
// write as it downloads and not load the whole file into memory.
func DownloadFile(filepath string, url string) error {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}