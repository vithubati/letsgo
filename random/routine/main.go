package main
import (
	"fmt"
	"sync"
)

const N = 10

func main() {
	m := make(map[int]int)

	wg := &sync.WaitGroup{}
	mu := &sync.Mutex{}
	wg.Add(N)
	for i := 0; i < N; i++ {
		go func(x int) {
			defer wg.Done()
			mu.Lock()
			fmt.Println("=====i is===: ", x)
			m[x] = x
			mu.Unlock()
		}(i)
	}
	wg.Wait()
	println(len(m))
	fmt.Println(m)
}