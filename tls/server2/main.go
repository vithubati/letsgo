package main

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func main() {
	addr := flag.String("addr", "localhost:3000", "HTTPS server address")
	certFile := flag.String("certfile", "cert.pem", "trusted CA certificate")
	flag.Parse()
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/hola", func(w http.ResponseWriter, r *http.Request) {
		cert, err := os.ReadFile(*certFile)
		if err != nil {
			log.Fatal(err)
		}
		certPool := x509.NewCertPool()
		if ok := certPool.AppendCertsFromPEM(cert); !ok {
			log.Fatalf("unable to parse cert from %s", *certFile)
		}

		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					RootCAs: certPool, // This is telling Go which certificates the client can trust.
				},
			},
		}

		res, err := client.Get("https://" + *addr + "/hello")
		if err != nil {
			log.Fatal(err)
		}
		defer res.Body.Close()

		html, err := io.ReadAll(res.Body)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%v\n", res.Status)
		fmt.Printf(string(html))
		w.Write(html)
	})
	http.ListenAndServe(":4000", r)
}
