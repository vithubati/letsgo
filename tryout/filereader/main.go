package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	readFile, err := os.Open("./filtered.csv")
	if err != nil {
		log.Fatal(err)
	}
	f, err := os.Create("./updated.sql")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		email := fileScanner.Text()
		slc := strings.Split(email, "@")
		slc2 := strings.Split(email, ".")
		newEmail := fmt.Sprintf("%s%s%s%s%s", slc[0], "@", slc[len(slc)-1], "-cars.invalid.", slc2[len(slc2)-1])
		q := fmt.Sprintf("UPDATE `drivelearn`.`users` SET `email` = '%s', `username` = '%s' WHERE email = '%s';\n", newEmail, newEmail, email)
		_, err := f.WriteString(q)
		if err != nil {
			log.Fatal(err)
		}
	}
	f.Sync()

	readFile.Close()
}
