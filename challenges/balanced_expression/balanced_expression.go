package main

import (
	stack2 "bitbucket.org/vithubati/letsgo/dsa/stack"
	"fmt"
	"strings"
)

func IsBalanced(chars string) bool {
	stack := stack2.StringStack()
	left := leftCharacters()
	right := rightCharacters()
	for _, c := range chars {
		if strings.Contains(left, string(c)) {
			stack.Push(string(c))
		}
		if strings.Contains(right, string(c)) {
			if stack.IsEmpty() {
				return false
			}
			last, ok := stack.Pop()
			if !ok {
				return false
			}
			if strings.Index(right, last) != strings.Index(left, string(c)) {
				return false
			}
		}
	}
	return stack.IsEmpty()
}

func leftCharacters() string {
	return "{<[("
}

func rightCharacters() string {
	return "}>])"
}

func main() {
	str := "{dfd}<fdf"
	fmt.Println(IsBalanced(str))
	str = "{dfd}<fdf>"
	fmt.Println(IsBalanced(str))
}
