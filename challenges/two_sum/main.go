package main

import (
	"fmt"
	"math/rand"
	"sort"
	"strings"
)

func twoSum(nums []int, target int) []int {
	for i := 0; i < len(nums); i++ {
		for j := 0; j < len(nums); i++ {
			if nums[i] == nums[j] {
				continue
			}
			if nums[i]+nums[j] == target {
				return []int{nums[i], nums[j]}
			}
		}
	}
	return []int{}
}

func twoSumWithMAp(nums []int, target int) []int {
	bucket := make(map[int]int)
	for i := 0; i < len(nums); i++ {
		need := target - nums[i]
		if val, ok := bucket[need]; ok {
			return []int{val, i}
		} else {
			bucket[nums[i]] = i
		}
	}
	return []int{}
}

func Solution(A []int) int {
	// write your code in Go 1.4
	//lst := []int{4, 5, 2, 8, 1, 9, 3}
	sort.Sort(sort.IntSlice(A))
	for i := 0; i < len(A); i++ {
		if A[i] < 1 {
			continue
		}
		if !contains(A[i]+1, A) {
			return A[i] + 1
		}
	}
	return 1
}
func contains(el int, nums []int) bool {
	for i := 0; i < len(nums); i++ {
		if nums[i] == el {
			return true
		}
	}
	return false
}

func occuranve(value string) int {
	// write your code in Go 1.4
	swap := 0
	for i, _ := range value {
		runLength := 1
		for ; i+1 < len(value) && string(value[i]) == string(value[i+1]); i++ {
			runLength++
		}
		swap += runLength / 3
	}
	return swap
}

//baaaaa
func occuranve2(value string) int {
	// write your code in Go 1.4
	swap := 0
	//last := ""
	for i := 0; i < len(value); {
		fmt.Println("i is: ", i)
		if i+2 < len(value) && string(value[i]) == string(value[i+1]) && string(value[i+1]) == string(value[i+2]) {
			//last = string(value[i+2])
			swap++
			i += 3
			continue
		}
		i++
	}
	return swap
}

func main() {
	nums := []int{2, 11, 7, 15}
	target := 9
	fmt.Println(twoSum(nums, target))
	//fmt.Println(twoSumWithMAp(nums, target))
	//lst := []int{4, 5, 2, 8, 1, 9, 3}
	//fmt.Println(Solution(lst))
	//fmt.Println(occuranve("baaaaa"))
	//fmt.Println(occuranve("baababb"))
	//fmt.Println(occuranve2("baaaaa"))
	//fmt.Println(occuranve2("baaabbaabbba"))
	//fmt.Println(occuranve2("baabab"))
	//fmt.Println(RandStringBytes(1))
	//fmt.Println(replace("??????s"))
}

func replace(riddle string) string {
	for {
		s := strings.Index(riddle, "?")
		r := RandStringBytes(1)
		if s == -1 {
			break
		}

		if s == 0 &&  s == len(riddle)-1{
			if compareR(string(riddle[s+1]), r) {
				riddle = strings.Replace(riddle, "?", r, 1)
				continue
			}
		}else if s == 0 {
			if compareR(string(riddle[s+1]), r) {
				riddle = strings.Replace(riddle, "?", r, 1)
				continue
			}
		} else if s == len(riddle)-1 {
			if compareL(string(riddle[s-1]), r) {
				riddle = strings.Replace(riddle, "?", r, 1)
				continue
			}
		}else{
			if compareLR(string(riddle[s-1]), string(riddle[s+1]), r) {
				//riddle = riddle[:s] + r + riddle[s+1:]
				riddle = strings.Replace(riddle, "?", r, 1)
				continue
			}
		}
	}
	return riddle
}
//riddle = riddle[:s] + r + riddle[s+1:]

func compareLR(l, r, s string) bool {
	if l != s && r != s {
		return true
	}
	return false
}
func compareL(l, s string) bool {
	if l != s {
		return true
	}
	return false
}
func compareR(r, s string) bool {
	if r != s {
		return true
	}
	return false
}

const letterBytes = "abcdefghijklmnopqrstuvwxyz"

func RandStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

//ss:= "Hello"
//fmt.Println(string(ss[1]))
//return 1
