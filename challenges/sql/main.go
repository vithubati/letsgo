package main

import (
	"database/sql/driver"
	"fmt"
	"sync"
)

type sql struct {
	driversMu sync.RWMutex
	drivers   map[string]driver.Driver
}

func NwqSql() sql {
	return sql{drivers: make(map[string]driver.Driver, 0)}
}
func (s sql) Drivers() map[string]driver.Driver {
	s.driversMu.RLock()
	defer s.driversMu.RUnlock()
	newD := make(map[string]driver.Driver, 0)
	for k, v := range s.drivers {
		newD[k] = v
	}
	return newD
}
func (s sql) register(name string, d driver.Driver) {
	s.drivers[name] = d
}

type Dummy struct {
	driver.Driver
}

func main() {
	s := NwqSql()
	fmt.Println(fmt.Sprintf("Drivers: %+v", s.Drivers()))
	s.register("sample", Dummy{})
	fmt.Println(fmt.Sprintf("Drivers: %+v", s.Drivers()))
	nsql := s.Drivers()
	nsql["test"] = Dummy{}
	fmt.Println(fmt.Sprintf("Drivers: %+v", nsql))
	fmt.Println(fmt.Sprintf("Drivers: %+v", s.Drivers()))

	//fmt.Println(fmt.Sprintf("Drivers: %p", &s.drivers))
	//fmt.Println(fmt.Sprintf("Drivers: %p", &nsql))
}
